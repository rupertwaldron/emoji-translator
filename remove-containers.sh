if [[ $(docker ps -aq) ]]; then
    echo "Removing containers"
    docker ps -aq | xargs docker stop | xargs docker rm
else
    echo "No containers were found"
fi
